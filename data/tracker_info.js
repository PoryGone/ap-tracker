// Games will be shown in the settings in order
let ordered_games = [
    crystal,
    crystalg,
    emerald,
    frlg_nosevii,
    frlg,
    platinum,
    hgss,
    hgss_pc,
    white2,
    white2ad,
    black2ad,
    white2_a,
    gen3crossover,
    crystal_fir,
    emerald_ex
];

// Used to name save files
const TRACKER_NAME = "pokemon-tracker";

// Provide a path to use as a savefile for testing
const TEST_SAVEFILE = "https://sekii.gitlab.io/pokemon-tracker/data/test_save1.txt";
